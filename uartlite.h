#ifndef UARTLITE_H_
#define UARTLITE_H_

#include "libxildrv.h"

#ifdef __cplusplus
extern "C" {
#endif

int uart_lite_clear(xildev *dev, uint32_t base_addr);
int uart_lite_tx(xildev *dev, uint32_t base_addr, uint8_t *tx_data, uint16_t num_bytes);
int uart_lite_rx(xildev *dev, uint32_t base_addr, uint8_t *rx_data, uint16_t max_num_bytes);

#ifdef __cplusplus
}
#endif

#endif /* UARTLITE_H_ */
