#include "libpcd.h"
#include "dma.h"
#include <unistd.h>

#define REG_MM2S_DMACR				0x00
#define REG_MM2S_DMASR				0x04
#define REG_MM2S_CURDESC			0x08
#define REG_MM2S_TAILDESC			0x10

#define REGISTER_TRANSLATION		0x30
#define DESCRIPTOR_SIZE				0x40

#define BIT_CR_RS					0
#define BIT_CR_RESET				2
#define BIT_CR_CYCLIC				4

#define BIT_SR_HALTED				0
#define BIT_SR_IDLE					1
#define BIT_SR_DMAINTERR			4
#define BIT_SR_DMASLVERR			5
#define BIT_SR_DMADECERR			6
#define BIT_SR_SGINTERR				8
#define BIT_SR_SGSLVERR				9
#define BIT_SR_SGDECERR				10

#define MASK_SR_ANY_ERROR	((1 << BIT_SR_DMAINTERR) | (1 << BIT_SR_DMASLVERR) | (1 << BIT_SR_DMADECERR) | \
							 (1 << BIT_SR_SGINTERR) | (1 << BIT_SR_SGSLVERR) | (1 << BIT_SR_SGDECERR))

#define DEV_ADDR_SPACE_OFFSET		0

//////////////////////////////////////////////////////////////////////////////
// Generic functions
//////////////////////////////////////////////////////////////////////////////

int dma_write_descriptor(xildev *dev, uint32_t bram_base_addr, uint8_t desc_no, uint8_t next_no, uint64_t addr, uint32_t length)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;

	uint32_t descriptor[] = {
		DEV_ADDR_SPACE_OFFSET + bram_base_addr + next_no * DESCRIPTOR_SIZE,
		0,
		addr & 0xFFFFFFFF,
		addr >> 32,
		0,
		0,
		length,
		0
	};
	
	const int descriptor_len = sizeof(descriptor) / sizeof(uint32_t);
	const uint32_t base_addr = bram_base_addr + desc_no * DESCRIPTOR_SIZE;
	for(int i = 0; i < descriptor_len; i++)
		if(xil_write_reg(dev, base_addr + i * 4, descriptor[i]) != 0)
			return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

//////////////////////////////////////////////////////////////////////////////
// Memory-mapped to Stream channel
//////////////////////////////////////////////////////////////////////////////

int dma_mm2s_reset(xildev *dev, uint32_t dma_base_addr)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(xil_write_reg(dev, dma_base_addr + REG_MM2S_DMACR, 1 << BIT_CR_RESET) != 0)
		return PCD_ERROR_REG_ACCESS;
	for(int i = 0; i < 20; i++)
	{
		usleep(1000);
		uint32_t reg_val = 0;
		if(xil_read_reg(dev, dma_base_addr + REG_MM2S_DMACR, &reg_val) != 0)
			return PCD_ERROR_REG_ACCESS;
		if(!(reg_val & (1 << BIT_CR_RESET)))
			return PCD_ERROR_NONE;
	}
	return PCD_ERROR_NO_RESPONSE;
}

int dma_mm2s_start(xildev *dev, uint32_t dma_base_addr, uint32_t bram_base_addr, uint8_t first_descriptor, uint8_t last_descriptor, bool cyclic)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(xil_write_reg(dev, dma_base_addr + REG_MM2S_CURDESC, DEV_ADDR_SPACE_OFFSET + bram_base_addr + first_descriptor * DESCRIPTOR_SIZE) != 0)
		return PCD_ERROR_REG_ACCESS;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, dma_base_addr + REG_MM2S_DMASR, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	if(reg_val & MASK_SR_ANY_ERROR)
	{
		int ret = dma_mm2s_reset(dev, dma_base_addr);
		if(ret != PCD_ERROR_NONE)
			return ret;
	}
	reg_val = 1 << BIT_CR_RS;
	if(cyclic)
		reg_val |= 1 << BIT_CR_CYCLIC;
	if(xil_write_reg(dev, dma_base_addr + REG_MM2S_DMACR, reg_val) != 0) // run
		return PCD_ERROR_REG_ACCESS;
	if(xil_write_reg(dev, dma_base_addr + REG_MM2S_TAILDESC, DEV_ADDR_SPACE_OFFSET + bram_base_addr + last_descriptor * DESCRIPTOR_SIZE) != 0)
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int dma_mm2s_stop(xildev *dev, uint32_t dma_base_addr)
{
	if(!dev)
		return PCD_ERROR_NULL_POINTER;
	if(xil_write_reg(dev, dma_base_addr + REG_MM2S_DMACR, 0) != 0) // stop
		return PCD_ERROR_REG_ACCESS;
	return PCD_ERROR_NONE;
}

int dma_mm2s_status(xildev *dev, uint32_t dma_base_addr, bool *running, bool *errors)
{
	if(!dev || !running || !errors)
		return PCD_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, dma_base_addr + REG_MM2S_DMASR, &reg_val) != 0)
		return PCD_ERROR_REG_ACCESS;
	*running = (reg_val & ((1 << BIT_SR_HALTED) | (1 << BIT_SR_IDLE))) == 0;
	*errors = (reg_val & MASK_SR_ANY_ERROR) != 0;
	return PCD_ERROR_NONE;
}

//////////////////////////////////////////////////////////////////////////////
// Stream to Memory-mapped channel
//////////////////////////////////////////////////////////////////////////////

int dma_s2mm_reset(xildev *dev, uint32_t dma_base_addr)
{
	return dma_mm2s_reset(dev, dma_base_addr + REGISTER_TRANSLATION);
}

int dma_s2mm_start(xildev *dev, uint32_t dma_base_addr, uint32_t bram_base_addr, uint8_t first_descriptor, uint8_t last_descriptor, bool cyclic)
{
	return dma_mm2s_start(dev, dma_base_addr + REGISTER_TRANSLATION, bram_base_addr, first_descriptor, last_descriptor, cyclic);
}

int dma_s2mm_stop(xildev *dev, uint32_t dma_base_addr)
{
	return dma_mm2s_stop(dev, dma_base_addr + REGISTER_TRANSLATION);
}

int dma_s2mm_status(xildev *dev, uint32_t dma_base_addr, bool *running, bool *errors)
{
	return dma_mm2s_status(dev, dma_base_addr + REGISTER_TRANSLATION, running, errors);
}
