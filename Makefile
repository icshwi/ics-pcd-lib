CCFLAGS := -Wall -O3 -std=gnu11 -g -ggdb
LDFLAGS := -Wall -O3

INCLUDES := . ../ics-xdriver-lib
TARGETS := libpcd.a pcd-tool
LIBS := -L. -lpcd -L../ics-xdriver-lib -lxildrv -lm
LIB_OBJ := libpcd.o uartlite.o dma.o
APP_OBJ := pcd-tool.o
HEADERS := $(wildcard *.h)

all: $(TARGETS)

clean:
	@$(RM) -f $(TARGETS) $(LIB_OBJ) $(APP_OBJ)

pcd-tool: pcd-tool.o libpcd.a $(HEADERS)
	$(CC) $(LDFLAGS) $< $(LIBS) -o $@

libpcd.so: $(LIB_OBJ) $(HEADERS)
	$(CC) $(LDFLAGS) -fPIC -shared $(LIB_OBJ) -o $@

libpcd.a: $(LIB_OBJ) $(HEADERS)
	ar rc $@ $(LIB_OBJ)

%.o: %.cpp
	$(CC) -c $(CCFLAGS) $(INCLUDES:%=-I%) $< -o $@

%.o: %.c
	$(CC) -c $(CCFLAGS) $(INCLUDES:%=-I%) $< -o $@
