#ifndef LIBPCD_H_
#define LIBPCD_H_

#include "libxildrv.h"
#include <stdbool.h>

#define ACT_CHANNELS	2

#ifdef __cplusplus
extern "C" {
#endif

enum PcdErrorType
{
	PCD_ERROR_NONE = 0,
	PCD_ERROR_NO_SUCH_CHANNEL = -1,
	PCD_ERROR_REG_ACCESS = -2,
	PCD_ERROR_NULL_POINTER = -3,
	PCD_ERROR_OUT_OF_RANGE = -4,
	PCD_ERROR_FILE_ACCESS = -5,
	PCD_ERROR_NO_RESPONSE = -6,
	PCD_ERROR_DMA_TRANSFER_FAILED = -7,
	PCD_WRONG_FIRMWARE_ID = -8,
};

enum PcdTriggerType
{
	PCD_TR_NONE = 0,
	PCD_TR_FREE = 1,
	PCD_TR_RISING_EDGE = 2,
	PCD_TR_FALLING_EDGE = 3,
	PCD_TR_ANY_EDGE = 4,
};

enum PcdTriggerLine
{
	PCD_TL_RX17 = 0,
	PCD_TL_TX17 = 1,
	PCD_TL_RX18 = 2,
	PCD_TL_TX18 = 3,
	PCD_TL_RX19 = 4,
	PCD_TL_TX19 = 5,
	PCD_TL_RX20 = 6,
	PCD_TL_TX20 = 7,
};

struct PcdSensorSampleStruct
{
	int16_t ch_a;
	int16_t ch_b;
	int16_t ch_c;
	int16_t ch_d;
};

typedef struct PcdSensorSampleStruct PcdSensorSample;

int pcd_init(xildev *dev);
const char * pcd_error_string(int error_code);

int pcd_mcu_clear(xildev *dev);
int pcd_mcu_tx(xildev *dev, uint8_t *tx_data, uint16_t num_bytes);
int pcd_mcu_rx(xildev *dev, uint8_t *rx_data, uint16_t max_num_bytes);
int pcd_mcu_set_dir(xildev *dev, int channel, bool actuator);
int pcd_mcu_get_dir(xildev *dev, int channel, bool *actuator);
int pcd_mcu_is_actuator(xildev *dev, int channel, bool *actuator);
int pcd_mcu_get_prot(xildev *dev, int channel, bool *tripped);
int pcd_mcu_reset_prot(xildev *dev, int channel);

int pcd_act_set_trigger_source(xildev *dev, enum PcdTriggerLine trig_line, enum PcdTriggerLine stop_line, uint8_t evt_no);
int pcd_act_get_trigger_source(xildev *dev, enum PcdTriggerLine *trig_line, enum PcdTriggerLine *stop_line, uint8_t *evt_no);
int pcd_act_get_triggers_per_line(xildev *dev, uint8_t *trig_count);
int pcd_act_trigger_arm(xildev *dev, int channel, enum PcdTriggerType trig, bool auto_rearm);
int pcd_act_get_trigger_type(xildev *dev, int channel, enum PcdTriggerType *trig, bool *auto_rearm);
int pcd_act_was_triggered(xildev *dev, int channel, bool *triggered);
int pcd_act_set_sample_count(xildev *dev, int channel, uint16_t sample_count);
int pcd_act_get_sample_count(xildev *dev, int channel, uint16_t *sample_count);
int pcd_act_set_trigger_delay(xildev *dev, int channel, double trig_delay);
int pcd_act_get_trigger_delay(xildev *dev, int channel, double *trig_delay);
int pcd_act_write_data(xildev *dev, int channel, int16_t *storage, uint32_t num_samples);
int pcd_act_dma_start(xildev *dev, int channel, uint32_t num_samples);
int pcd_act_dma_stop(xildev *dev, int channel);

int pcd_sen_set_trigger_source(xildev *dev, enum PcdTriggerLine trig_line, enum PcdTriggerLine stop_line, uint8_t evt_no);
int pcd_sen_get_trigger_source(xildev *dev, enum PcdTriggerLine *trig_line, enum PcdTriggerLine *stop_line, uint8_t *evt_no);
int pcd_sen_get_triggers_per_line(xildev *dev, uint8_t *trig_count);
int pcd_sen_trigger_arm(xildev *dev, enum PcdTriggerType trig, bool auto_rearm);
int pcd_sen_get_trigger_type(xildev *dev, enum PcdTriggerType *trig, bool *auto_rearm);
int pcd_sen_get_trigger_info(xildev *dev, uint8_t *trig_len, uint8_t *trig_cnt);
int pcd_sen_was_triggered(xildev *dev, bool *triggered);
int pcd_sen_set_sample_count(xildev *dev, uint16_t sample_count);
int pcd_sen_get_sample_count(xildev *dev, uint16_t *sample_count);
int pcd_sen_set_trigger_delay(xildev *dev, double trig_delay);
int pcd_sen_get_trigger_delay(xildev *dev, double *trig_delay);
int pcd_sen_set_pre_trigger(xildev *dev, bool enabled);
int pcd_sen_get_pre_trigger(xildev *dev, bool *enabled);
int pcd_sen_set_sample_dropping(xildev *dev, uint8_t samples_to_drop);
int pcd_sen_get_sample_dropping(xildev *dev, uint8_t *samples_to_drop);
int pcd_sen_read_prepare(xildev *dev, uint32_t num_samples);
int pcd_sen_read_check(xildev *dev, bool *running);
int pcd_sen_read_finalize(xildev *dev);
int pcd_sen_read_transfer(xildev *dev, PcdSensorSample *storage, uint32_t num_samples);
int pcd_sen_read_data(xildev *dev, PcdSensorSample *storage, uint32_t num_samples, unsigned int timeout);

#ifdef __cplusplus
}
#endif

#endif /* LIBPCD_H_ */
