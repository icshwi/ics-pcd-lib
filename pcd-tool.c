#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <getopt.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>

#include "libpcd.h"

#define ACTUATOR_GAIN		(0x7FFF / 187.4)

#define NUM_LINES			8
const char *trigger_lines[NUM_LINES] = {"rx17", "tx17", "rx18", "tx18", "rx19", "tx19", "rx20", "tx20"};

void print_usage(char *prog_name)
{
	printf("\n");
	printf("ESS Piezo Control Device test utility\n");
	printf("\n");
	printf("General arguments:\n");
	printf("    -x <xdma_file> - Use the specified xdma file, default is /dev/xdma0\n");
	printf("    -h             - Print this help\n");
	printf("    -M <command>   - Exchange serial data with the diagnostic MCU\n");
	printf("    -m <mode>      - Switch channel to the desired mode (sen/act)\n");
	printf("    -p             - Print current configuration\n");
	printf("\n");
	printf("Piezo actuator arguments:\n");
	printf("    -a <type>      - Auto-repeat trigger, type can be: R,F,B,I\n");
	printf("    -c <channel>   - Use specified device channel: A or B\n");
	printf("    -d <time>      - Set the desired delay after trigger (ms)\n");
	printf("    -l <spec>      - Set the M-LVDS line configuration (see below)\n");
	printf("    -r             - Reset the piezo protection circuit\n");
	printf("    -w <spec>      - Load the selected waveform (see below)\n");
	printf("    -t <type>      - Arm the trigger, type can be: R,F,B,I\n");
	printf("\n");
	printf("Piezo sensor arguments:\n");
	printf("    -A <type>      - Auto-repeat trigger, type can be: R,F,B,I\n");
	printf("    -C <file_name> - Use the provided CSV file name, default is out.csv\n");
	printf("    -D <time>      - Set the desired delay after trigger (ms)\n");
	printf("    -G <count>     - Try to read given number of samples from buffer\n");
	printf("    -L <spec>      - Set the M-LVDS line configuration (see below)\n");
	printf("    -N             - Try to print captured data using gnuplot\n");
	printf("    -P <enable>    - Enable or disable the pretrigger (on/off)\n");
	printf("    -R <number>    - Set the number of samples to drop between two stored ones\n");
	printf("    -S <count>     - Set the number of samples to capture after trigger\n");
	printf("    -T <type>      - Arm the trigger, type can be: R,F,B,I\n");
	printf("\n");
	printf("Debug options:\n");
	printf("    -0             - Print DMA descriptors\n");
	printf("    -1             - Print DMA status registers\n");
	printf("    -2             - Trigger diagnostics\n");
	printf("\n");
	printf("Supported trigger types:\n");
	printf(" R - rising edge\n");
	printf(" F - falling edge\n");
	printf(" B - both edges\n");
	printf(" I - immediate\n");
	printf(" N - none\n");
	printf("\n");
	printf("Waveform specification:\n");
	printf("<type>[,a=<amplitude>][,o=<offset>][,f=<frequency>][,t=<period>]\n");
	printf("Waveform types: sin, tri, sqr. Other parameters in SI units.\n");
	printf("Period and frequency are interchangeable. Avoid spaces.\n");
	printf("\n");
	printf("Line configuration specification:\n");
	printf("<trigger line>,<clear line>,<event number>\n");
	printf("Trigger lines can be: rx17, tx17, rx18, tx18, rx19, tx19, rx20, tx20.\n");
	printf("The selected unit will be triggered on <event number>'th event (1..15)\n");
	printf("on the <trigger line> after an event on <clear line>. Avoid spaces.\n");
	printf("\n");
	printf("Examples:\n");
	printf(" - Getting diagnostic MCU status:\n");
	printf("   %s -M s\n", prog_name);
	printf(" - Setting actuator for the LLRF_START and LLRF_END triggers:\n");
	printf("   %s -l tx17,tx20,1\n", prog_name);
	printf(" - Loading a sine wave of amplitude 5 V, offset 0 V and frequency 1 kHz:\n");
	printf("   %s -w sin,a=5,o=0,f=1e3\n", prog_name);
}

void msleep(unsigned int milliseconds)
{
	struct timespec st;
	st.tv_sec = milliseconds / 1000;
	st.tv_nsec = (milliseconds % 1000) * 1000000L;
	nanosleep(&st, NULL);
}

int diag_talk(xildev *dev, const char *cmd)
{
	uint8_t buf[1024];
	int tx_r, rx_r, status = 0;
	printf("Request: \"%s\"\n", cmd);
	snprintf((char*)buf, sizeof(buf), "%s\r\n", cmd);
	tx_r = pcd_mcu_tx(dev, (uint8_t*)buf, strlen((char*)buf));
	if(tx_r != 0)
	{
		printf("Warning: TX status = %d\n", tx_r);
		status = tx_r;
	}
	
	msleep(30);
	printf("Response: \"");
	do
	{
		msleep(20);
		rx_r = pcd_mcu_rx(dev, buf, sizeof(buf) - 1);
		if(rx_r < 0)
		{
			printf("Warning: RX status = %d\n", rx_r);
			status = rx_r;
		}
		if(rx_r > 0)
		{
			buf[rx_r] = '\0';
			printf("%s", (char*)buf);
		}
	}
	while(rx_r > 0);
	printf("\"\n");
	return status;
}

void print_error(int code)
{
	printf("> %s\n", pcd_error_string(code));
}

int print_conf(xildev *dev, int ch)
{
	int res;
	int minor = -1;
	int major = -1;
	int patch = -1;
	
	{
		uint32_t base_addr, value;
		res = xil_read_reg(dev, 0x110, &base_addr);
		if(res != 0)
			printf("Cannot read register at 0x110 - error: %s\n", strerror(errno));
		else
		{
			res = xil_read_reg(dev, base_addr + 4, &value);
			if(res != 0)
				printf("Cannot read register at 0x%08X - error: %s\n", base_addr + 4, strerror(errno));
			else
			{
				major = (value >> 24) & 0xFF;
				minor = (value >> 8) & 0xFFFF;
				patch = value & 0xFF;
				printf("Custom Logic version: %d.%d.%d\n", major, minor, patch);
			}
		}
	}
	
	if(major > 0 && minor > 0)
	{	// introduced in 1.1.0
		enum PcdTriggerLine trig_line, stop_line;
		uint8_t evt_no, trig_count;
		int res2;
		printf("\n=> Generator trigger configuration:\n");
		res  = pcd_act_get_trigger_source(dev, &trig_line, &stop_line, &evt_no);
		res2 = pcd_act_get_triggers_per_line(dev, &trig_count);
		if(res == PCD_ERROR_NONE && res2 == PCD_ERROR_NONE)
			printf("Event line: %s\nClear line: %s\nEvent number: %d (of measured %d)\n",
					trigger_lines[trig_line], trigger_lines[stop_line], evt_no, trig_count);
		else
			print_error(res != PCD_ERROR_NONE ? res : res2);
		printf("\n=> Sensor trigger configuration:\n");
		res  = pcd_sen_get_trigger_source(dev, &trig_line, &stop_line, &evt_no);
		res2 = pcd_sen_get_triggers_per_line(dev, &trig_count);
		if(res == PCD_ERROR_NONE && res2 == PCD_ERROR_NONE)
			printf("Event line: %s\nClear line: %s\nEvent number: %d (of measured %d)\n",
					trigger_lines[trig_line], trigger_lines[stop_line], evt_no, trig_count);
		else
			print_error(res != PCD_ERROR_NONE ? res : res2);
		printf("\n");
	}
	
	for(int channel = 0; channel < ACT_CHANNELS; channel++)
	{
		printf("=> Channel %c\n", 'A' + channel);
		
		printf("Desired function: ");
		bool desired_dir;
		res = pcd_mcu_get_dir(dev, channel, &desired_dir);
		if(res == PCD_ERROR_NONE)
			printf("%s\n", desired_dir ? "actuator" : "sensor");
		else
			print_error(res);
		
		printf("Actual function: ");
		bool actual_dir;
		res = pcd_mcu_is_actuator(dev, channel, &actual_dir);
		if(res == PCD_ERROR_NONE)
			printf("%s\n", actual_dir ? "actuator" : "sensor");
		else
			print_error(res);
		
		printf("Protection status: ");
		bool prot_tripped;
		res = pcd_mcu_get_prot(dev, channel, &prot_tripped);
		if(res == PCD_ERROR_NONE)
			printf("%s\n", prot_tripped ? "tripped" : "OK");
		else
			print_error(res);
		
		printf("Delay after trigger: ");
		double delay;
		res = pcd_act_get_trigger_delay(dev, channel, &delay);
		if(res == PCD_ERROR_NONE)
			printf("%0.5lf ms\n", delay * 1000.0);
		else
			print_error(res);
		
		printf("Trigger occurred: ");
		bool done;
		res = pcd_act_was_triggered(dev, channel, &done);
		if(res == PCD_ERROR_NONE)
			printf("%s\n", done ? "yes" : "no");
		else
			print_error(res);
		
		enum PcdTriggerType trig;
		bool auto_rearm;
		res = pcd_act_get_trigger_type(dev, channel, &trig, &auto_rearm);
		if(res == PCD_ERROR_NONE)
			printf("Trigger: %c, auto: %s\n", "NIRFB"[(int)trig], auto_rearm ? "yes" : "no");
		else
			print_error(res);
		
		printf("\n");
	}
	
	if(ch < 0)
	{
		printf("=> Sensor channel\n");
		printf("Pre-trigger: ");
		bool enabled;
		res = pcd_sen_get_pre_trigger(dev, &enabled);
		if(res == PCD_ERROR_NONE)
			printf("%s\n", enabled ? "enabled" : "disabled");
		else
			print_error(res);
		
		printf("Sample count: ");
		uint16_t sample_count;
		res = pcd_sen_get_sample_count(dev, &sample_count);
		if(res == PCD_ERROR_NONE)
			printf("%d\n", sample_count);
		else
			print_error(res);
		
		printf("Delay after trigger: ");
		double delay;
		res = pcd_sen_get_trigger_delay(dev, &delay);
		if(res == PCD_ERROR_NONE)
			printf("%0.5lf ms\n", delay * 1000.0);
		else
			print_error(res);
		
		printf("Samples to drop: ");
		uint8_t samples_to_drop;
		res = pcd_sen_get_sample_dropping(dev, &samples_to_drop);
		if(res == PCD_ERROR_NONE)
			printf("%d\n", samples_to_drop);
		else
			print_error(res);
		
		printf("Trigger occurred: ");
		bool done;
		res = pcd_sen_was_triggered(dev, &done);
		if(res == PCD_ERROR_NONE)
			printf("%s\n", done ? "yes" : "no");
		else
			print_error(res);
		
		enum PcdTriggerType trig;
		bool auto_rearm;
		res = pcd_sen_get_trigger_type(dev, &trig, &auto_rearm);
		if(res == PCD_ERROR_NONE)
			printf("Trigger: %c, auto: %s\n", "NIRFB"[(int)trig], auto_rearm ? "yes" : "no");
		else
			print_error(res);
		printf("\n");
	}
	
	if(major > 0 && minor == 0)
	{	// not longer operational since 1.1.0
		uint8_t trig_len, trig_cnt;
		res = pcd_sen_get_trigger_info(dev, &trig_len, &trig_cnt);
		if(res == PCD_ERROR_NONE)
			printf("Trigger length: %0.2f us, trigger count: %d\n", trig_len / 100.0f, trig_cnt);
		else
			print_error(res);
	}
	return 0;
}

int set_int(xildev *dev, int ch, const char *value_txt, char which)
{
	char *endptr = NULL;
	int value = strtol(value_txt, &endptr, 0);
	if(endptr == value_txt)
	{
		printf("Cannot parse '%s' as integer value\n", value_txt);
		return 1;
	}
	
	int res = 0;
	if(isupper(which))
	{ //sensor
		switch(which)
		{
			case 'R': res = pcd_sen_set_sample_dropping(dev, value); break;
			case 'S': res = pcd_sen_set_sample_count(dev, value); break;
			default: return 2;
		}
		print_error(res);
	}

	return 0;
}

int set_double(xildev *dev, int ch, const char *value_txt, char which)
{
	char *endptr = NULL;
	double value = strtod(value_txt, &endptr);
	if(endptr == value_txt)
	{
		printf("Cannot parse '%s' as floating point value\n", value_txt);
		return 1;
	}
	
	int res = 0;
	if(isupper(which))
	{ //sensor
		switch(which)
		{
			case 'D': res = pcd_sen_set_trigger_delay(dev, value / 1000.0); break;
			default: return 2;
		}
		print_error(res);
	}
	else
	{ //actuator
		for(int channel = 0; channel < ACT_CHANNELS; channel++)
		{
			if(channel != ch && ch != -1)
				continue;
			
			printf("\nChannel %c\n", 'A' + channel);
			
			int res = 0;
			switch(which)
			{
				case 'd': res = pcd_act_set_trigger_delay(dev, channel, value / 1000.0); break;
				default: return 2;
			}
			print_error(res);
		}
	}
	
	return 0;
}

int set_bool(xildev *dev, int ch, const char *value_txt, char which)
{
	bool is_false = false, is_true = false;
	switch(which)
	{
		case 'm':
			is_true = (!strcmp(value_txt, "act") || !strcmp(value_txt, "actuator") || !strcmp(value_txt, "driver") || !strcmp(value_txt, "out"));
			is_false = (!strcmp(value_txt, "sen") || !strcmp(value_txt, "sensor") || !strcmp(value_txt, "in"));
			break;
		default:
			is_true = (!strcmp(value_txt, "on") || !strcmp(value_txt, "yes") || !strcmp(value_txt, "true") || !strcmp(value_txt, "enable"));
			is_false = (!strcmp(value_txt, "off") || !strcmp(value_txt, "no") || !strcmp(value_txt, "false") || !strcmp(value_txt, "disable"));
			break;
	}
	
	if(!is_true && !is_false)
	{
		printf("Cannot interpret '%s' in context of option '-%c'\n", value_txt, which);
		return 1;
	}
	
	bool value = is_true;
	int res = 0;
	
	if(isupper(which))
	{ //sensor
		switch(which)
		{
			case 'P': res = pcd_sen_set_pre_trigger(dev, value); break;
			default: return 2;
		}
		print_error(res);
	}
	else
	{ //actuator
		for(int channel = 0; channel < ACT_CHANNELS; channel++)
		{
			if(channel != ch && ch != -1)
				continue;
			
			printf("\nChannel %c\n", 'A' + channel);
			switch(which)
			{
				case 'm': res = pcd_mcu_set_dir(dev, channel, value); break;
				default: return 2;
			}
			print_error(res);
		}
	}

	return 0;
}

int prot_reset(xildev *dev, int ch)
{
	for(int channel = 0; channel < ACT_CHANNELS; channel++)
	{
		if(channel != ch && ch != -1)
			continue;
		
		printf("\nChannel %c\n", 'A' + channel);
		int res = pcd_mcu_reset_prot(dev, channel);
		print_error(res);
	}

	return 0;
}

int arm_trigger(xildev *dev, int ch, const char *value_txt, char which)
{
	if(strlen(value_txt) != 1)
	{
		printf("Trigger specification shall consist of a single character\n");
		return 1;
	}
	
	char c = tolower(value_txt[0]);

	enum PcdTriggerType tt = PCD_TR_NONE;
	if(c == 'r')
		tt = PCD_TR_RISING_EDGE;
	else if(c == 'f')
		tt = PCD_TR_FALLING_EDGE;
	else if(c == 'b')
		tt = PCD_TR_ANY_EDGE;
	else if(c == 'i')
		tt = PCD_TR_FREE;
	else if(c == 'n')
		tt = PCD_TR_NONE;
	else
	{
		printf("Unknown trigger type '%c'\n", value_txt[0]);
		return 1;
	}
	
	bool auto_rearm = (which == 'A') || (which == 'a');
	int res = 0;
	if(isupper(which))
	{ //sensor
		uint16_t sample_count;
		if(pcd_sen_get_sample_count(dev, &sample_count) == PCD_ERROR_NONE)
		{
			if(sample_count == 0)
			{
				printf("Please set the number of samples to capture\n");
				return 1;
			}
		}
		res = pcd_sen_trigger_arm(dev, tt, auto_rearm);
		print_error(res);
	}
	else
	{ //actuator
		for(int channel = 0; channel < ACT_CHANNELS; channel++)
		{
			if(channel != ch && ch != -1)
				continue;
			
			printf("\nChannel %c\n", 'A' + channel);
			res = pcd_act_trigger_arm(dev, channel, tt, auto_rearm);
			print_error(res);
		}
	}
	return 0;
}

int get_data(xildev *dev, const char *value_txt, const char *out_file)
{
	char *endptr = NULL;
	int num_samples = strtol(value_txt, &endptr, 0);
	if(endptr == value_txt)
	{
		printf("Cannot parse '%s' as integer value\n", value_txt);
		return 1;
	}
	
	if(num_samples <= 0)
	{
		printf("Number of samples have to be positive integer\n");
		return 1;
	}
	
	size_t req_size = num_samples * sizeof(PcdSensorSample);
	const size_t alignment = 4096;
	size_t alloc_size = alignment * ((req_size + alignment - 1) / alignment);
	PcdSensorSample * ss = (PcdSensorSample*)aligned_alloc(alignment, alloc_size);
	int res = pcd_sen_read_data(dev, ss, num_samples, 200);
	print_error(res);
	
	if(res != PCD_ERROR_NONE)
	{
		free(ss);
		return 1;
	}
	
	FILE * f = fopen(out_file, "w");
	if(f == NULL)
	{
		printf("Cannot open output file '%s'\n", out_file);
		free(ss);
		return 1;
	}
	
	for(int s_no = 0; s_no < num_samples; s_no++)
	{
		PcdSensorSample * s = &ss[s_no];
		fprintf(f, "%5d, %5d, %5d, %5d\n", s->ch_a, s->ch_b, s->ch_c, s->ch_d);
	}
	fclose(f);

	free(ss);
	return 0;
}

int lvds_config(xildev *dev, const char *const_spec, char which)
{
	char * tok;
	char spec[128];
	const char delims[] = " ,";
	int tok_cnt = 0;
	int trig_line = -1, clear_line = -1, evt_no = -1;
	strncpy(spec, const_spec, sizeof(spec));
	tok = strtok(spec, delims);
	while(tok)
	{
		tok_cnt++;
		if(tok_cnt < 3)
		{
			int line = -1;
			for(int i = 0; i < NUM_LINES; i++)
				if(!strcmp(tok, trigger_lines[i]))
					line = i;
			if(tok_cnt == 1)
				trig_line = line;
			else if(tok_cnt == 2)
				clear_line = line;
		}
		else if(tok_cnt == 3)
		{
			evt_no = strtol(tok, NULL, 10);
		}
		tok = strtok(NULL, delims);
	}
	
	if(tok_cnt != 3)
	{
		printf("Wrong syntax of the M-LVDS line configuration specification\n");
		return 1;
	}
	
	if(trig_line < 0)
	{
		printf("Cannot parse trigger line specification\n");
		return 1;
	}

	if(clear_line < 0)
	{
		printf("Cannot parse clear line specification\n");
		return 1;
	}
	
	if(evt_no < 1 || evt_no > 15)
	{
		printf("Event number has to be in range 1..15\n");
		return 1;
	}
	
	int res;
	if(which == 'l')
	{
		res = pcd_act_set_trigger_source(dev, trig_line, clear_line, evt_no);
		print_error(res);
	}
	else if(which == 'L')
	{
		res = pcd_sen_set_trigger_source(dev, trig_line, clear_line, evt_no);
		print_error(res);
	}
	
	return 0;
}

int load_wave(xildev *dev, int ch, const char *const_spec)
{
	enum {
		W_SIN,
		W_TRI,
		W_SQR
	} wave = W_SIN;
	
	float ampl = 5.0f;
	float freq = 1e3f;
	float offset = 0.0f;
	
	char * tok;
	char spec[128];
	const char delims[] = " ,";
	strncpy(spec, const_spec, sizeof(spec));
	tok = strtok(spec, delims);
	while(tok)
	{
		char * param = strchr(tok, '=');
		if(param)
		{
			*param = 0;
			param++;
			
			char *endptr = NULL;
			double value = strtof(param, &endptr);
			if(endptr == param)
			{
				printf("Cannot parse '%s' as floating point value\n", param);
				return 1;
			}
			
			if(!strcmp(tok, "a"))
				ampl = value;
			else if(!strcmp(tok, "t"))
				freq = (value > 0.0) ? 1.0 / value : -1.0;
			else if(!strcmp(tok, "o"))
				offset = value;
			else if(!strcmp(tok, "f"))
				freq = value;
			else
			{
				printf("Unknown waveform parameter '%s'\n", tok);
				return 1;
			}
		}
		else if(!strcmp(tok, "sin"))
			wave = W_SIN;
		else if(!strcmp(tok, "tri"))
			wave = W_TRI;
		else if(!strcmp(tok, "sqr"))
			wave = W_SQR;
		else
		{
			printf("Unknown waveform type '%s'\n", tok);
			return 1;
		}
		
		tok = strtok(NULL, delims);
	}
	
	if(ampl < 0.0)
	{
		printf("Amplitude cannot be lower than 0\n");
		return 1;
	}
	if(freq < 10.0)
	{
		printf("Frequency cannot be lower than 10 Hz\n");
		return 1;
	}
	if(freq > 100e3)
	{
		printf("Frequency cannot be higher 100 kHz\n");
		return 1;
	}
	if(offset > 100.0 || offset < -100.0)
	{
		printf("Offset cannot be higher 100 V\n");
		return 1;
	}
	
	int num_samples = round(1e6 / freq);
	num_samples &= ~1; //make it even
	int num_samples2 = num_samples / 2;
	
	double * dblsamples = (double*)malloc(num_samples * sizeof(double));
	
	switch(wave)
	{
	case W_SIN:
		{
			double w = 2.0 * M_PI / (double)num_samples;
			for(int i = 0; i < num_samples; i++)
				dblsamples[i] = sinf(w * i);
			printf("Generating sine wave");
		}
		break;
	case W_TRI:
		{
			double m = 2.0 / (double)(num_samples2 - 1);
			for(int i = 0; i < num_samples2; i++)
					dblsamples[i] = dblsamples[num_samples - 1 - i] = 1.0 - (double)i * m;
			printf("Generating triangle wave");
		}
		break;
	case W_SQR:
		{
			for(int i = 0; i < num_samples2; i++)
			{
				dblsamples[i] = -1.0;
				dblsamples[i+num_samples2] = 1.0;
			}
			printf("Generating square wave");
		}
		break;
	default:
		printf("Generating death rays");
	}
	
	printf(" of amplitude %f V, offset: %f V and frequency: %f Hz\n", ampl, offset, freq);
	printf("Number of samples: %d\n", num_samples);
	
	int16_t * samples = (int16_t*)malloc(num_samples * sizeof(int16_t));
	for(int i = 0; i < num_samples; i++)
	{
		int v = round((dblsamples[i] * ampl + offset) * ACTUATOR_GAIN);
		if(v > INT16_MAX)
			v = INT16_MAX;
		else if(v < INT16_MIN)
			v = INT16_MIN;
		samples[i] = v;
	}
	free(dblsamples);
	
	for(int channel = 0; channel < ACT_CHANNELS; channel++)
	{
		if(channel != ch && ch != -1)
			continue;
		printf("\nChannel %c\n", 'A' + channel);
		
		int res = 0;
		
		enum PcdTriggerType trig;
		bool auto_rearm;
		res = pcd_act_get_trigger_type(dev, channel, &trig, &auto_rearm);
		if(res == PCD_ERROR_NONE)
		{
			if(trig != PCD_TR_NONE)
			{
				printf("Disabling trigger\n");
				res = pcd_act_trigger_arm(dev, channel, PCD_TR_NONE, false);
				print_error(res);
				msleep(100); //wait for any pending DMA transfer to be terminated
			}
		}
		
		printf("Stopping DMA\n");
		res = pcd_act_dma_stop(dev, channel);
		print_error(res);
		
		printf("Loading samples\n");
		res = pcd_act_write_data(dev, channel, samples, num_samples);
		print_error(res);
		
		printf("Configuring reader\n");
		res = pcd_act_set_sample_count(dev, channel, num_samples);
		print_error(res);
		
		printf("Starting DMA\n");
		res = pcd_act_dma_start(dev, channel, num_samples);
		print_error(res);
	}
	
	free(samples);
	return 0;
}

int debug(xildev *dev, char which)
{
	if(which == '0')
	{
		//      D[0x000C0000] = { 0x00000000, 0x00000000, 0x00000000, 0x00000000 }
		printf(" <descr_addr>     <next desc> <buff addr>  <control>   <status>\n");
		int descriptors[] = {0, 0x10, 0x20};
		for(int i = 0; i < sizeof(descriptors)/sizeof(descriptors[0]); i++)
		{	// BRAM_BASE_ADDRESS + DESCRIPTOR_SIZE * descr_no
			uint32_t base = 0x0C0000 + 0x40 * descriptors[i];
			struct {
				uint32_t nxtdesc;
				uint32_t buffer_address;
				uint32_t control;
				uint32_t status;
			} descr;
			xil_read_reg(dev, base + 0x00, &descr.nxtdesc);
			xil_read_reg(dev, base + 0x08, &descr.buffer_address);
			xil_read_reg(dev, base + 0x18, &descr.control);
			xil_read_reg(dev, base + 0x1C, &descr.status);
			printf("D[0x%08X] = { 0x%08X, 0x%08X, 0x%08X, 0x%08X }\n", base, descr.nxtdesc, descr.buffer_address, descr.control, descr.status);
		}
	}
	else if(which == '1')
	{
		struct {
			uint32_t cr;
			uint32_t sr;
		} dma[3];
		xil_read_reg(dev, 0x0C2000 + 0x30, &dma[0].cr);
		xil_read_reg(dev, 0x0C2000 + 0x34, &dma[0].sr);
		xil_read_reg(dev, 0x0C4000 + 0x00, &dma[1].cr);
		xil_read_reg(dev, 0x0C4000 + 0x04, &dma[1].sr);
		xil_read_reg(dev, 0x0C6000 + 0x00, &dma[2].cr);
		xil_read_reg(dev, 0x0C6000 + 0x04, &dma[2].sr);
		printf("             <CR>        <SR>\n");
		//      DMA[0] = { 0x00010002, 0x00010009}
		for(int i = 0; i < 3; i++)
			printf("DMA[%d] = { 0x%08X, 0x%08X}\n", i, dma[i].cr, dma[i].sr);
	}
	else if(which == '2')
	{
		uint8_t trig_len, trig_cnt;
		int old_cnt = -1, cnt = 0;
		struct timeval t_start, t_now, t_last, t_diff;
		gettimeofday(&t_now, NULL);
		t_start = t_last = t_now;
		printf("Trigger after ");
		while(1)
		{
			msleep(1);
			gettimeofday(&t_now, NULL);
			timersub(&t_now, &t_start, &t_diff);
			if(t_diff.tv_sec >= 5)
				break;
			int res = pcd_sen_get_trigger_info(dev, &trig_len, &trig_cnt);
			if(res != PCD_ERROR_NONE)
			{
				print_error(res);
				return 0;
			}
			if(old_cnt >= 0)
			{
				if(old_cnt != (int)trig_cnt)
				{
					if(cnt)
					{
						timersub(&t_now, &t_last, &t_diff);
						int tim_ms = t_diff.tv_sec * 1000 + (t_diff.tv_usec + 500) / 1000;
						printf("%d ", tim_ms);
						fflush(stdout);
					}
					cnt++;
					t_last = t_now;
				}
			}
			old_cnt = trig_cnt;
		}
		timersub(&t_now, &t_start, &t_diff);
		int total_ms = t_diff.tv_sec * 1000 + (t_diff.tv_usec + 500) / 1000;
		printf("ms\nDetected %d triggers in %d ms (of %d expected)\n", cnt, total_ms, (total_ms * 14 + 500) / 1000);
	}
	else
	{
		printf("There is nothing to see here...\n");
	}
	return 0;
}

int plot(const char *file_name)
{
	const char fmt[] = "gnuplot -persist -e \"set xlabel \\\"Sample number\\\"; set ylabel \\\"ADC Counts\\\"; plot \\\"%s\\\" using 1 with line title \\\"CH 1\\\", \\\"%s\\\" using 2 with line title \\\"CH 2\\\", \\\"%s\\\" using 3 with line title \\\"CH 3\\\", \\\"%s\\\" using 4 with line title \\\"CH 4\\\";\"";
	char buf[1024];
	snprintf(buf, sizeof(buf), fmt, file_name, file_name, file_name, file_name);
	return system(buf);
}

int main(int argc, char *argv[])
{
	xildev *dev = NULL;
	int opt, result = -1, ch = -1;
	const char * dev_name = "/dev/xdma0";
	const char * csv_file_name = "out.csv";
	const char avail_options[] = "0123456789x:hm:M:pa:c:d:l:w:rt:A:C:D:G:L:NP:R:S:T:";
	
	if(argc == 1)
	{
		printf("No arguments were given. Please execute \"%s -h\" for help.\n", argv[0]);
		return 0;
	}
	
	// Read configuration, handle getting help
	while((opt = getopt(argc, argv, avail_options)) != -1)
	{
		switch(opt)
		{
			case 'h':
				print_usage(argv[0]);
				return 0;
			case 'x':
				dev_name = optarg;
				break;
			case '?':
				print_usage(argv[0]);
				return -1;
		}
	}
	
	dev = xil_open_device(dev_name);
	if(!dev)
	{
		fprintf(stderr, "Cannot open file '%s' due to '%s'\n", dev_name, strerror(errno));
		return -1;
	}
	else
	{
		int init_s = pcd_init(dev);
		if(init_s != PCD_ERROR_NONE)
		{
			printf("Warning: PCD initialization error - ");
			print_error(init_s);
			printf("Most probable causes:\n");
			printf(" - dead PCIe link (maybe reboot is needed?)\n");
			printf(" - wrong board selected (consider passing -x option)\n");
			xil_close_device(dev);
			return -1;
		}
	}
	
	// Perform tasks
	optind = 1;
	while((opt = getopt(argc, argv, avail_options)) != -1)
	{
		switch(opt)
		{
		case 'c':
			if(!strcmp(optarg, "a") || !strcmp(optarg, "A") || !strcmp(optarg, "0"))
				ch = 0;
			else if (!strcmp(optarg, "b") || !strcmp(optarg, "B") || !strcmp(optarg, "1"))
				ch = 1;
			else
			{
				printf("Unrecognized channel specification '%s'\n", optarg);
				return -1;
			}
			break;
		case 'C':
			csv_file_name = optarg;
			break;
		case 'M':
			result = diag_talk(dev, optarg);
			break;
		case 'p':
			result = print_conf(dev, ch);
			break;
		case 'D':
		case 'd':
			result = set_double(dev, ch, optarg, opt);
			break;
		case 'l':
		case 'L':
			result = lvds_config(dev, optarg, opt);
			break;
		case 'w':
			result = load_wave(dev, ch, optarg);
			break;
		case 'N':
			result = plot(csv_file_name);
			break;
		case 'G':
			result = get_data(dev, optarg, csv_file_name);
			break;
		case 'R':
		case 'S':
			result = set_int(dev, ch, optarg, opt);
			break;
		case 'r':
			result = prot_reset(dev, ch);
			break;
		case 'm':
		case 'P':
			result = set_bool(dev, ch, optarg, opt);
			break;
		case 'a':
		case 'A':
		case 't':
		case 'T':
			result = arm_trigger(dev, ch, optarg, opt);
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			result = debug(dev, opt);
			break;
		}
	}
	
	xil_close_device(dev);
	return result;
}
